using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace TccSys.Runtime
{
    [Serializable]
    public class SequentialCmd : ICmd
    {
        [SerializeReference, SubclassSelector] ICmd[] _cmds;
        
        public async UniTask ExecuteAsync(CancellationToken cancellationToken)
        {
            foreach (ICmd cmd in _cmds)
            {
                await cmd.ExecuteAsync(cancellationToken);
            }
        }
    }
}