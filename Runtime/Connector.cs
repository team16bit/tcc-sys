using System;
using System.Threading;
using UniRx;
using UnityEngine;

namespace TccSys.Runtime
{
    [Serializable]
    public class Connector : IConnector
    {
        [SerializeReference, SubclassSelector] ITrigger _when;
        [SerializeReference, SubclassSelector] ICmd _execute;

        IDisposable _disposable;
        
        public virtual void Connect()
        {
            _disposable = _when.Subscribe(_ => _execute.ExecuteAsync(_cancellationToken));
        }

        public virtual void Disconnect()
        {
            _disposable?.Dispose();
        }

        protected virtual CancellationToken _cancellationToken => default;
    }
}