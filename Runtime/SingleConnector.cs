using UnityEngine;

namespace TccSys.Runtime
{
    public class SingleConnector : MonoBehaviour
    {
        [SerializeReference, SubclassSelector] IConnector _connector;

        void OnEnable()
        {
            _connector.Connect();
        }

        void OnDisable()
        {
            _connector.Disconnect();
        }
    }
}