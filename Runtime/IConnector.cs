namespace TccSys.Runtime
{
    public interface IConnector
    {
        void Connect();
        void Disconnect();
    }
}