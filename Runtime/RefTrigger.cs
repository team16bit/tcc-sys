using System;
using UniRx;
using UnityEngine;

namespace TccSys.Runtime
{
    [Serializable]
    public class RefTrigger : ITrigger
    {
        [SerializeField] Trigger _ref;
        
        public IDisposable Subscribe(IObserver<Unit> observer)
        {
            return _ref.Subscribe(observer);
        }

        public void Fire()
        {
            _ref.Fire();
        }
    }
}