using System;
using UniRx;

namespace TccSys.Runtime
{
    public interface ITrigger : IObservable<Unit>
    {
        void Fire();
    }
}