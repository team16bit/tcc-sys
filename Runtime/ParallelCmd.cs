using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace TccSys.Runtime
{
    [Serializable]
    public class ParallelCmd : ICmd
    {
        [SerializeReference, SubclassSelector] ICmd[] _cmds;
        
        public UniTask ExecuteAsync(CancellationToken cancellationToken)
        {
            return UniTask.WhenAll(_cmds.Select(x => x.ExecuteAsync(cancellationToken)));
        }
    }
}