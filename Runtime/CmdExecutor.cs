using UnityEngine;

namespace TccSys.Runtime
{
    public class CmdExecutor : MonoBehaviour
    {
        [SerializeReference, SubclassSelector] ICmd[] _cmds;

        void Start()
        {
            foreach (ICmd cmd in _cmds)
            {
                cmd.ExecuteAsync();
            }
        }
    }
}