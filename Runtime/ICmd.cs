using System.Threading;
using Cysharp.Threading.Tasks;

namespace TccSys.Runtime
{
    public interface ICmd
    {
        UniTask ExecuteAsync(CancellationToken cancellationToken = default);
    }
}