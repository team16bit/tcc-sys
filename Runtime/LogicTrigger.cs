using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace TccSys.Runtime
{
    public abstract class LogicTrigger : ITrigger
    {
        [SerializeField] Trigger[] _triggers;
        
        IObservable<Unit> _observable;
        IObservable<Unit> observable => _observable ??= Triggers2Observable(_triggers);
        protected abstract IObservable<Unit> Triggers2Observable(IEnumerable<Trigger> triggers);
        
        public IDisposable Subscribe(IObserver<Unit> observer)
        {
            return observable.Subscribe(observer);
        }

        void ITrigger.Fire()
        {
            throw new NotImplementedException();
        }
    }
}