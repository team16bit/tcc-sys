using System;
using System.Collections.Generic;
using UniRx;

namespace TccSys.Runtime
{
    [Serializable]
    public class OrTrigger : LogicTrigger
    {
        protected override IObservable<Unit> Triggers2Observable(IEnumerable<Trigger> triggers)
        {
            return triggers.Merge();
        }
    }
}