using System;
using NaughtyAttributes;
using UniRx;
using UnityEngine;

namespace TccSys.Runtime
{
    public abstract class Trigger : MonoBehaviour, ITrigger
    {
        Subject<Unit> _subject;
        IObservable<Unit> _observable => _subject ??= new Subject<Unit>();

        public IDisposable Subscribe(IObserver<Unit> observer)
        {
            return _observable.Subscribe(observer);
        }
        
        [Button(nameof(Fire))]
        public void Fire()
        {
            _subject?.OnNext(Unit.Default);
        }
    }
}