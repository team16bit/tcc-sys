using UnityEngine;

namespace TccSys.Runtime
{
    public class ConnectorHub : MonoBehaviour
    {
        [SerializeReference, SubclassSelector] IConnector[] _connectors;
        
        void OnEnable()
        {
            foreach (IConnector connector in _connectors)
            {
                connector.Connect();    
            }
        }

        void OnDisable()
        {
            foreach (IConnector connector in _connectors)
            {
                connector.Disconnect();    
            }
        }
    }
}